using System;
using Newtonsoft.Json;

namespace ShapeGuesser.Models
{
    // TODO: to implement a reader for this data,
    // use JsonConvert.DeserializeObject<UserData>(string data)
    // where 'data' is the data from a user file.
    public class UserData
    {
        [JsonProperty("n")]
        public string UserName { get; set; } 
        [JsonProperty("p")]
        public byte[] PasswordHash { get; set; }
        [JsonProperty("s")]
        public byte[] PasswordSalt { get; set; }
        [JsonProperty("u")]
        public UsageData[] AppUsage { get; set; }
    }
    public class UsageData
    {
        [JsonProperty("l")]
        public DateTimeOffset LoginTime { get; set; }
        [JsonProperty("u")]
        public TimeSpan Usage { get; set; }
        [JsonProperty("s")]
        public int Score { get; set;}
    }
}
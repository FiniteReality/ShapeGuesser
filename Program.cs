﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Linq;

using Newtonsoft.Json;

using ShapeGuesser.Models;
using System.Collections.Generic;

// To whomever may be questioning my decision to not use a GUI:
// I can't exactly write my own GUI toolkit in C# which supports
// .NET Core... At least in the asinine timespan I gave myself
// (2 days) anyway

namespace ShapeGuesser
{
    public class Program
    {
        // Stores the location of user data.
        const string USER_DATA_STORE = "./data"; // TODO: change this based on OS

        Random rng;

        Program()
        {
            rng = new Random(Environment.TickCount);
        }

        // Main entry point
        public static void Main(string[] args)
        {
            Console.Title = "ShapeGuesser";
            (new Program()).Run();
        }

        // Writes the app header.
        void WriteAppHeader()
        {
            Console.Clear();
            Console.WriteLine(@"--- Shape Guesser ---");
            Console.WriteLine($"Version 1.0.3");
            Console.WriteLine(@"---------------------");
        }

        // Reads a "secure" string from the console.
        string ReadPasswordSecure(char mask = '*')
        {
            StringBuilder text = new StringBuilder();
            while (true)
            {
                var keyInfo = Console.ReadKey(true);

                if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    if (text.Length > 0)
                    {
                        text.Remove(text.Length - 1, 1);
                        Console.CursorLeft -= 1;
                        Console.Write(' ');
                        Console.CursorLeft -= 1;
                    }
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    return text.ToString();
                }
                else
                {
                    text.Append(keyInfo.KeyChar);
                    Console.Write(mask);
                }
            }
        }

        // Verifies a login, by checking username and password.
        UserData VerifyLogin(string username, string password)
        {
            // Check their username is safe
            if (username.IndexOfAny(Path.GetInvalidFileNameChars()) > 0)
            {
                Console.WriteLine("Username contained invalid characters. Try again.");
                return null;
            }

            UserData info = null;
            // Try and retrieve their info
            try
            {
                string path = Path.Combine(USER_DATA_STORE, $"{username}.json");

                using (var file = File.OpenText(path))
                {
                    info = JsonConvert.DeserializeObject<UserData>(file.ReadToEnd());
                }
            }
            catch (FileNotFoundException e)
            {
#if DEBUG
                Console.WriteLine($"Error: Could not find file: {e}");
#else
                Console.WriteLine("That user does not exist. Try again.");
#endif
                return null;
            }

            // Ensure their password is correct
            using (Rfc2898DeriveBytes pwd = new Rfc2898DeriveBytes(password, info.PasswordSalt, 10000))
            {
                var hashed = pwd.GetBytes(128);
                int result = 0;
                for (int i = 0; i < hashed.Length; i++)
                {
                    result |= hashed[i] ^ info.PasswordHash[i];
                }

                if (result != 0)
                {
                    Console.WriteLine("Invalid password. Try again.");
                    return null;
                }
            }

            // If we get this far, the password was correct.
            return info;
        }

        // Allows the user to make a choice between multiple options. Returns the option chosen.
        int Choose(string info, params string[] options)
        {
            while (true)
            {
                Console.WriteLine(info);
                for (int i = 0; i < options.Length; i++)
                {
                    Console.WriteLine($"{i+1}. {options[i]}");
                }

                Console.Write("Choice: ");
                string input = Console.ReadLine();
                int result;
                if (!int.TryParse(input, out result))
                    Console.WriteLine("Invalid choice. Try again.");
                else if (!(result > 0 && result <= options.Length))
                    Console.WriteLine("Invalid choice. Try again.");
                else
                    return --result;
            }
        }

        // Handles a login
        UserData HandleLogin()
        {
            UserData login = null;
            while (login == null)
            {
                Console.Write("Username: ");
                string username = Console.ReadLine();
                Console.Write("Password: ");
                string password = ReadPasswordSecure();

                login = VerifyLogin(username, password);
            }

            return login;
        }

        // Handles a registration
        UserData HandleRegister()
        {
            string username = "";
            while (true)
            {
                Console.Write("Username: ");
                username = Console.ReadLine();

                if (username.IndexOfAny(Path.GetInvalidFileNameChars()) > 0)
                    Console.WriteLine("That username contains invalid characters. Try again.");
                else if (File.Exists(Path.Combine(USER_DATA_STORE, $"{username}.json")))
                    Console.WriteLine("That user already exists. Try again.");
                else
                    break;
            }

            string password = "";
            while (true)
            {
                Console.Write("Password: ");
                password = ReadPasswordSecure();
                Console.Write("Retype password: ");
                string password2 = ReadPasswordSecure();

                if (password != password2)
                    Console.WriteLine("The passwords entered did not match. Try again.");
                else
                    break;
            }

            try
            {
                string path = Path.Combine(USER_DATA_STORE, $"{username}.json");
                using (var file = File.OpenWrite(path))
                {
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        using (Rfc2898DeriveBytes pwd = new Rfc2898DeriveBytes(password, 32))
                        {
                            pwd.IterationCount = 10000;
                            byte[] hash = pwd.GetBytes(128);
                            byte[] salt = pwd.Salt;

                            UserData info = new UserData();
                            info.PasswordHash = hash;
                            info.PasswordSalt = salt;
                            info.UserName = username;
                            writer.Write(JsonConvert.SerializeObject(info));

                            return info;
                        }
                    }
                }
            }
            catch (IOException e)
            {
#if DEBUG
                Console.WriteLine($"Error: {e}");
#else
                Console.WriteLine("An unknown error occured.");
#endif
            }

            return null;
        }

        // Handles the "guessing" of a shape.
        int GuessShape(double area)
        {
            var answers = new double[]{area, area, area, area};
            for (int i = 0; i < 3; i++)
                answers[i] += rng.NextDouble() * rng.Next(10, 1000);

            answers = answers.OrderBy(x => rng.NextDouble()).ToArray();

            var sortedAnswers = answers.Select(x => x.ToString("0.##cm")).ToArray();

            var guess = 0;
            while (guess < 2)
            {
                var choice = Choose("Its area could be:", sortedAnswers);
                if (answers[choice] == area)
                {
                    Console.WriteLine("That's right!");
                    break;
                }
                else
                {
                    Console.WriteLine("Sorry, that's incorrect. Try again!");
                    guess++;
                }
            }

            if (guess == 2)
                Console.WriteLine($"Sorry, you guessed incorrectly. The correct answer is {area}");

            Console.WriteLine();
            return 2 - guess;
        }

        // Prompts the user to guess the area of a square
        int GuessSquare()
        {
            var side = rng.NextDouble() * rng.Next(1, 1000);
            var area = side * side;
            Console.WriteLine($"I'm thinking of a square, with side {side:0.##}cm...");

            return GuessShape(area);
        }

        // Prompts the user to guess the area of a rectangle
        int GuessRectangle()
        {
            var sideW = rng.NextDouble() * rng.Next(1, 1000);
            var sideH = rng.NextDouble() * rng.Next(1, 1000);
            var area = sideW * sideH;
            Console.WriteLine($"I'm thinking of a rectangle, with sides {sideW:0.##}cm by {sideH:0.##}...");

            return GuessShape(area);
        }

        // Prompts the user to guess the area of a circle
        int GuessCircle()
        {
            var radius = rng.NextDouble() * rng.Next(1, 1000);
            var area = Math.PI * radius * radius;
            Console.WriteLine($"I'm thinking of a circle, with radius {radius:0.##}cm...");

            return GuessShape(area);
        }

        // Prompts the user to guess the area of a triangle
        int GuessTriangle()
        {
            var sideW = rng.NextDouble() * rng.Next(1, 1000);
            var sideH = rng.NextDouble() * rng.Next(1, 1000);
            var area = 0.5 * sideW * sideH;
            Console.WriteLine($"I'm thinking of a triangle, with width {sideW:0.##}cm and height {sideH:0.##}cm...");

            return GuessShape(area);
        }

        // Handles startup code and handling state
        void Run()
        {
            WriteAppHeader();
            int choice = Choose("Please choose an option:", "Login", "Register", "Exit");

            UserData login = null;
            
            switch(choice)
            {
                case 0:
                    login = HandleLogin();
                    break;
                case 1:
                    login = HandleRegister();
                    break;
                case 2:
                    Console.WriteLine("Bye!");
                    Environment.Exit(0);
                    break;
            }

            // Store login time so we can save it later
            DateTimeOffset loginTime = DateTimeOffset.Now;
            Console.WriteLine("Successfully logged in!");

            var score = 0;
            var running = true;
            while (running)
            {
                Console.WriteLine($"Current score: {score}");
                choice = Choose("Please choose a shape or exit:", "square", "circle", "rectangle", "triangle", "Exit program");
                switch (choice)
                {
                    case 0: // square
                        score += GuessSquare();
                        break;
                    case 2: // rectangle
                        score += GuessRectangle();
                        break;
                    case 1: // circle
                        score += GuessCircle();
                        break;
                    case 3: // triangle
                        score += GuessTriangle();
                        break;
                    case 4: // Exit
                        running = false;
                        break;
                }
            }

            // the user exited here so we save their score/etc.
            Console.WriteLine($"Saving: {login?.UserName}");

            // We use this (highly) innefficient method because Newtonsoft.Json doesn't support serializing/deserializing List<T>
            var appUsage = login.AppUsage?.ToList() ?? new List<UsageData>();
            appUsage.Add(new UsageData{
                LoginTime = loginTime,
                Usage = (DateTimeOffset.Now - loginTime),
                Score = score
            });
            login.AppUsage = appUsage.ToArray();
            
            // Actually save our changes to disk
            try
            {
                string path = Path.Combine(USER_DATA_STORE, $"{login.UserName}.json");
                using (FileStream stream = File.OpenWrite(path))
                {
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        writer.Write(JsonConvert.SerializeObject(login));
                    }
                }
            }
            catch (IOException)
            {
                Console.WriteLine("Error saving data. Data may be lost.");
                // We can't do anything at this point other than tell the user
                // Retrying the save probably won't work either
            }
        }
    }
}
